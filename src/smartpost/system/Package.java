/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;
 
import java.util.ArrayList;
 
/**
 *
 * @author Ratio
 */
public class Package {
    protected  String packageclass;
    protected  String weight;
    protected  String name;
    protected  String fragile;
    protected  String width;
    protected  String height;
    protected  String depth;
    protected  String sendingCity;
    protected  String destinationCity;
    protected  String sendingMachine;
    protected  String destinationMachine;
    protected  Object object;
   
   
    public Package(String w, String n, String f, String sc, String dc, String sm,
            String dm,String wi, String he, String de, Object ob){
       
        weight = w;
        name = n;
        fragile = f;
        sendingCity = sc;
        destinationCity = dc;
        sendingMachine = sm;
        destinationMachine = dm;
        width = wi;
        height = he;
        depth = de;
        object=ob;
 
    }
    public Package(){ //constructor for weight and sizecheck purposes
        
    }
 
    public String getSendingCity(){
        return this.sendingCity;
    }
   
    public String getDestinationCity() {
        return this.destinationCity;
    }
   
    public String getSendingMachine() {
        return this.sendingMachine;
    }
   
    public String getDestinationMachine() {
        return this.destinationMachine;
    }
   
    
    public String getName(){
        return name;
    }
    
      

   
   
    public float calculateVolume(String width, String height, String depth){
        float volume = Float.valueOf(width) * Float.valueOf(height) * Float.valueOf(depth);
        return (volume / 1000); //returns volume in litres
    }
}
 
 
class firstClass extends Package {
    private int weightLimit = 20;
    private int sizeLimit = 500; //volume of the object (in litres)
    
    public firstClass(String w, String n, String f, String sc, String dc, String sm,
            String dm,String wi, String he, String de){
        
        super(w, n, f, sc, dc, sm, dm, wi, he, de,new Object(n, w, wi, he, de, f));
       
    }
    public firstClass(){
        super();
    }
    public int getWeightLimit() {
        return weightLimit;
    }
    
    public int getSizeLimit(){
        return sizeLimit;
    }
}
    
 
class secondClass extends Package {
        private int weightLimit = 5;
        private int sizeLimit = 50; //volume of the object (in litres)
    public secondClass(String w, String n, String f, String sc, String dc, String sm,
            String dm,String wi, String he, String de){
       
        super(w, n, f, sc, dc, sm, dm, wi, he, de,new Object(n, w, wi, he, de, f));

    }
    public secondClass(){
        super();
             
    }
    
    public int getWeightLimit() {
        return weightLimit;
    }
    
    public int getSizeLimit() {
        return sizeLimit;
    }

}
 
 
class thirdClass extends Package {
    private int weightLimit = 30;
    private int sizeLimit = 2000; //volume of the object (in litres)
    public thirdClass(String w, String n, String f, String sc, String dc, String sm,
            String dm,String wi, String he, String de){
        super(w, n, f, sc, dc, sm, dm, wi, he, de,new Object(n, w, wi, he, de, f));

    }
    public thirdClass(){
        super();
    }
    
    public int getWeightLimit(){
        return weightLimit;
    
    }
    public int getSizeLimit() {
        return sizeLimit;
    }
    
}



