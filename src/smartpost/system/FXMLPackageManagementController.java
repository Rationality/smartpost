/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Simo
 */
public class FXMLPackageManagementController implements Initializable {
    @FXML
    private TextField objectNameField;
    @FXML
    private TextField objectWeightField;
    @FXML
    private ComboBox<String> defaultObjects;
    @FXML
    private ComboBox<String> sendingCityCombobox;
    @FXML
    private ComboBox<String> destinationCityCombobox;
    @FXML
    private ComboBox<String> sendingMachineCombobox;
    @FXML
    private ComboBox<String> destinationMachineCombobox;
    @FXML
    private Button confirmCitiesButton;
    @FXML
    private Button confirmMachineButton;
    @FXML
    private Button cancelMachineButton;
    
    @FXML
    private Label cityErrorLabel;
    @FXML
    private Button presetItemSelectButton;
    @FXML
    private ToggleButton firstClassToggle;
    @FXML
    private ToggleButton secondClassToggle;
    @FXML
    private ToggleButton thirdClassToggle;
    @FXML
    private Label classLabel;
    @FXML
    private Button backToFirstChoiseButton;
    @FXML
    private CheckBox fragileCheck;

    @FXML
    private Button classInfoButton;
    @FXML
    private Button createNewObjectButton;
    @FXML
    private Tab itemSelectionTab;
    @FXML
    private Button confirmItemSelectionButton;
    @FXML
    private Tab classSelectionTab;
    @FXML
    private Tab locationSelectionTab;
    @FXML
    private Label objectErrorLabel;
    
    @FXML
    private Tab reviewTab;
    @FXML
    private TextArea reviewTextArea;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button confirmItemButton;
    @FXML
    private TabPane myTabPane;
    @FXML
    private Button backToFirstChoiseButton2;
    @FXML
    private Button moveToItemTabButton;
    @FXML
    private Button moveToLocationTabButton;
    @FXML
    private Button backToBeginningButton;
    @FXML
    private Button backToLocationTabButton;
    @FXML
    private Label classErrorLabel;
    @FXML
    private Button backToClassSelectionButton;
    @FXML
    private TextField objectWidthField;
    @FXML
    private TextField objectHeightField;
    @FXML
    private TextField objectDepthField;
    
    XMLtoSmartPost smartpost = XMLtoSmartPost.getInstance();
    Mainclass main = Mainclass.getInstance();
    
//temp variables used in package creating process:
    private String itemChoiceCheck; //checks if user chooses default item or creates a new item
    private String packageClassCheck;
    private String tempName;
    private String tempWeight;
    private String tempWidth;
    private String tempHeight;
    private String tempDepth;
    private String tempSendingCity;
    private String tempDestinationCity;
    private String tempSendingMachine;
    private String tempDestinationMachine;
    private String fragile;
    private firstClass firstClassCheck=new firstClass();
    private secondClass secondClassCheck=new secondClass();
    private thirdClass thirdClassCheck=new thirdClass();
    
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        //Setting names of city and smartpost locations to comboboxes
        sendingCityCombobox.getItems().addAll(smartpost.getCityList());
        destinationCityCombobox.getItems().addAll(smartpost.getCityList());
        //setting which tabs can be accessed
        classSelectionTab.setDisable(true);
        locationSelectionTab.setDisable(true);
        reviewTab.setDisable(true);
        
        //setting non-visible components 
        backToFirstChoiseButton.setVisible(false);
        objectNameField.setVisible(false);
        objectWeightField.setVisible(false);
        objectWidthField.setVisible(false);
        objectDepthField.setVisible(false);
        objectHeightField.setVisible(false);
        defaultObjects.setVisible(false);
        fragileCheck.setVisible(false);
        confirmItemSelectionButton.setVisible(false);
        confirmItemButton.setVisible(false);
        backToFirstChoiseButton2.setDisable(true);
        backToFirstChoiseButton2.setVisible(false);

        //Setting default items to combobox
        for(int i = 0; i < main.getDefaultObjectsList().size(); i++){
            defaultObjects.getItems().add( main.getDefaultObjectsList().get(i).getName());
        }
    }   


    @FXML
    private void classInfoAction(ActionEvent event) {
        //Opens new window for package class information
        try {
            Stage webview = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageClassInfo.fxml"));
            Scene scene = new Scene(page);
            webview.setMaxHeight(578);
            webview.setMinHeight(578);
            webview.setMaxWidth(936);
            webview.setMinWidth(936);
            webview.setScene(scene);
            webview.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @FXML
    private void confirmCitiesAction(ActionEvent event) {     
        
        if((sendingCityCombobox.getValue()!=null)&&(destinationCityCombobox.getValue()!=null)){
            ArrayList<Smartpost> smartposts = smartpost.getSmartpostList();
            sendingCityCombobox.setDisable(true);
            destinationCityCombobox.setDisable(true);
            sendingMachineCombobox.setDisable(false);
            destinationMachineCombobox.setDisable(false);
            cancelMachineButton.setDisable(false);
            confirmMachineButton.setDisable(false);
            confirmCitiesButton.setDisable(true);
            sendingMachineCombobox.getItems().clear();
            destinationMachineCombobox.getItems().clear();

            for (Smartpost s : smartposts){
                if(s.getCity().equals(sendingCityCombobox.getValue())){
                    sendingMachineCombobox.getItems().add(s.getPostoffice());
                    
                }
            }
            for(Smartpost p : smartposts){
                if(p.getCity().equals(destinationCityCombobox.getValue())){
                    destinationMachineCombobox.getItems().add(p.getPostoffice());
                }
            }
        }
        else{
            cityErrorLabel.setText("Syötä molemmat kaupungit.");
        }
    }

    @FXML
    private void confirmMachineAction(ActionEvent event) {
        
        if((sendingMachineCombobox.getValue()!=null)&&(destinationMachineCombobox.getValue()!=null)){
            if(sendingMachineCombobox.getValue().equals(destinationMachineCombobox.getValue())){
            cityErrorLabel.setText("Et voi lähettää pakettia lähetysautomaattiin.");
            }
            else{
                myTabPane.getSelectionModel().select(reviewTab);
                reviewTab.setDisable(false);
                sendingMachineCombobox.setDisable(true);
                destinationMachineCombobox.setDisable(true);
                sendingCityCombobox.setDisable(false);
                destinationCityCombobox.setDisable(false);
                confirmCitiesButton.setDisable(false);
                locationSelectionTab.setDisable(true);
                sendingCityCombobox.setDisable(false);
                destinationCityCombobox.setDisable(false);
                sendingMachineCombobox.setDisable(true);
                destinationMachineCombobox.setDisable(true);
                confirmMachineButton.setDisable(true);
                cancelMachineButton.setDisable(true);

                tempSendingCity = sendingCityCombobox.getValue();
                tempDestinationCity = destinationCityCombobox.getValue();
                tempSendingMachine = sendingMachineCombobox.getValue();
                tempDestinationMachine = destinationMachineCombobox.getValue();

                sendingMachineCombobox.getItems().clear();
                destinationMachineCombobox.getItems().clear();

                reviewTextArea.clear();
                reviewTextArea.setText("Paketin tiedot: \n\nLähetettävä esine: "+
                        tempName+"\nPaketin luokka: "+packageClassCheck+"\n"+ fragile+
                        "\nLähtökaupunki: "+tempSendingCity+"\nLähtöautomaatti: "+tempSendingMachine+
                        "\nKohdekaupunki: "+tempDestinationCity+"\nKohdeautomaatti: "+tempDestinationMachine);
            }
        }
        else{
            cityErrorLabel.setText("Syötä molemmat pakettiautomaatit.");
        }
        
  
    }

   
    
    @FXML
    private void cancelMachineAction(ActionEvent event) { 
        //goes back to choosing cities 
            sendingCityCombobox.setDisable(false);
            destinationCityCombobox.setDisable(false);
            sendingMachineCombobox.setDisable(true);
            destinationMachineCombobox.setDisable(true);
            confirmMachineButton.setDisable(true);
            cancelMachineButton.setDisable(true);
            sendingMachineCombobox.getItems().clear();
            destinationMachineCombobox.getItems().clear();
            confirmCitiesButton.setDisable(false);
            
            
            
    }

    @FXML
    private void presetItemSelectAction(ActionEvent event) {
        itemChoiceCheck = "default item";
        //enable selecting from default objects, only visual selection
        defaultObjects.setVisible(true);
        defaultObjects.setDisable(false);
        confirmItemSelectionButton.setDisable(false);
        confirmItemSelectionButton.setVisible(true);
        presetItemSelectButton.setDisable(true);
        createNewObjectButton.setDisable(true);
        backToFirstChoiseButton2.setDisable(false);
        backToFirstChoiseButton2.setVisible(true);
        
    }

    @FXML
    private void backToFirstAction(ActionEvent event) {
        //goes back to choosing from default items or new item
        presetItemSelectButton.setDisable(false);
        createNewObjectButton.setDisable(false);
        //disable selection fields, visibility
        defaultObjects.setVisible(false);
        defaultObjects.setDisable(true);
        confirmItemSelectionButton.setDisable(true);
        confirmItemSelectionButton.setVisible(false);
        objectNameField.setVisible(false);
        objectWeightField.setVisible(false);
        objectWidthField.setVisible(false);
        objectDepthField.setVisible(false);
        objectHeightField.setVisible(false);
        fragileCheck.setVisible(false);
        fragileCheck.setDisable(true);
        backToFirstChoiseButton.setDisable(true);
        backToFirstChoiseButton.setVisible(false);
        confirmItemButton.setDisable(true);
        confirmItemButton.setVisible(false);
        backToFirstChoiseButton2.setDisable(true);
        backToFirstChoiseButton2.setVisible(false);
    }

    @FXML
    private void createNewObjectAction(ActionEvent event) {
        //choose to create a new object instead of choosing from defaults
        itemChoiceCheck = "new item";

        objectWidthField.clear();
        objectHeightField.clear();
        objectDepthField.clear();
        objectNameField.clear();
        objectWeightField.clear();
        
        presetItemSelectButton.setDisable(true);
        createNewObjectButton.setDisable(true);
        //enable fields for creating new package
        objectNameField.setVisible(true);
        objectNameField.setDisable(false);
        objectWeightField.setVisible(true);
        objectWeightField.setDisable(false);
        objectWidthField.setVisible(true);
        objectDepthField.setVisible(true);
        objectHeightField.setVisible(true);
        classInfoButton.setVisible(true);
        fragileCheck.setVisible(true);
        fragileCheck.setDisable(false);
        backToFirstChoiseButton.setDisable(false);
        backToFirstChoiseButton.setVisible(true);
        confirmItemButton.setDisable(false);
        confirmItemButton.setVisible(true);
     

        
    }

    @FXML
    private void confirmItemAction(ActionEvent event) {
        //confirms given info on new item
        //checks that all required information is given by user
        if ((!objectNameField.getText().equals("")) && (!objectWeightField.getText().equals(""))
                && (!objectWidthField.getText().equals("")) && (!objectHeightField.getText().equals(""))
                && (!objectDepthField.getText().equals(""))) {

            if ((!objectWeightField.getText().matches("[0-9]*"))
                    || (!objectWidthField.getText().matches("[0-9]*"))
                    || (!objectHeightField.getText().matches("[0-9]*"))
                    || (!objectDepthField.getText().matches("[0-9]*"))) {

                objectErrorLabel.setText("Tiedot ovat virheelliset(Anna numeeriset tiedot kokonaislukuina).");
            } else {
                myTabPane.getSelectionModel().select(classSelectionTab);
                classSelectionTab.setDisable(false);
                itemSelectionTab.setDisable(true);
                //set visibility as it was when window was opened
                presetItemSelectButton.setDisable(false);
                createNewObjectButton.setDisable(false);
                //disable selection fields
                defaultObjects.setVisible(false);
                defaultObjects.setDisable(true);
                confirmItemSelectionButton.setDisable(true);
                confirmItemSelectionButton.setVisible(false);
                objectNameField.setVisible(false);
                objectWeightField.setVisible(false);
                objectWidthField.setVisible(false);
                objectDepthField.setVisible(false);
                objectHeightField.setVisible(false);
                fragileCheck.setVisible(false);
                backToFirstChoiseButton.setDisable(true);
                backToFirstChoiseButton.setVisible(false);
                confirmItemButton.setDisable(true);
                confirmItemButton.setVisible(false);
                backToFirstChoiseButton2.setDisable(true);
                backToFirstChoiseButton2.setVisible(false);
                tempName = objectNameField.getText();
                tempWeight = objectWeightField.getText();
                tempWidth = objectWidthField.getText();
                tempHeight = objectHeightField.getText();
                tempDepth = objectDepthField.getText();
            }
            if (fragileCheck.isSelected() == true) {
                fragile = "Särkyvää";
            } else if (fragileCheck.isSelected() == false) {
                fragile = "Ei särkyvää";
            }
        } else {
            objectErrorLabel.setText("Täytä kaikki kohdat ensin.");
        }
    }

    @FXML
    private void createPackageAction(ActionEvent event) {
        
        Storage storage=Storage.getInstance();
        String fragile = "Ei särkyvää";
        if (itemChoiceCheck.equals("new item")) {
            storage.addPackage(tempName, tempWeight, tempWidth, tempHeight, tempDepth,
                    fragile, packageClassCheck, tempSendingCity, tempDestinationCity,
                    tempSendingMachine, tempDestinationMachine);

        }
        if (itemChoiceCheck.equals("default item")) {
            storage.addPackage(tempName,tempWeight,tempWidth,tempHeight,tempDepth,
                    fragile, packageClassCheck,tempSendingCity,tempDestinationCity,
                    tempSendingMachine, tempDestinationMachine);
        }
        Stage stage = (Stage) createPackageButton.getScene().getWindow();
        stage.close();

    }

    @FXML
    private void moveToItemTabAction(ActionEvent event) {
        //goes back to first tab where items are chosen
        myTabPane.getSelectionModel().select(itemSelectionTab);
        classSelectionTab.setDisable(true);
        itemSelectionTab.setDisable(false);
    }

    @FXML
    private void moveToLocationTabButton(ActionEvent event) { //confirms class selection and moves on
        float tempSize= Float.valueOf(tempWidth)*Float.valueOf(tempHeight)*Float.valueOf(tempDepth)/1000;
        
        if(firstClassToggle.isSelected()==true){


                    myTabPane.getSelectionModel().select(locationSelectionTab);
                    classSelectionTab.setDisable(true);
                    locationSelectionTab.setDisable(false);
                    classErrorLabel.setText("");
            
        }
        
        if(secondClassToggle.isSelected()==true){
               
            myTabPane.getSelectionModel().select(locationSelectionTab);
            classSelectionTab.setDisable(true);
            locationSelectionTab.setDisable(false);
            classErrorLabel.setText("");
        }

            
        
        if(thirdClassToggle.isSelected()==true){

            myTabPane.getSelectionModel().select(locationSelectionTab);
            classSelectionTab.setDisable(true);
            locationSelectionTab.setDisable(false);
                classErrorLabel.setText("");
        }

            
        
        
        else{
            classErrorLabel.setText("Valitse postitusluokka ensin.");
            
        }
}
    
    

    @FXML
    private void firstClassSelectionAction(ActionEvent event) {
        float tempSize= Float.valueOf(tempWidth)*Float.valueOf(tempHeight)*Float.valueOf(tempDepth)/1000; //volume in litres
        if ((Float.valueOf(tempWeight) <= firstClassCheck.getWeightLimit())
                && (Float.valueOf(tempSize) <= firstClassCheck.getSizeLimit())) {
            packageClassCheck = "1";
            secondClassToggle.setSelected(false);
            thirdClassToggle.setSelected(false);
            classErrorLabel.setText("");
        }
        else{
            firstClassToggle.setSelected(false);
            classErrorLabel.setText("Esine ei sovi tähän luokkaan, lisää tietoa infonapista.");
        }
    }

    @FXML
    private void secondClassSelectionAction(ActionEvent event) {
        float tempSize= Float.valueOf(tempWidth)*Float.valueOf(tempHeight)*Float.valueOf(tempDepth)/1000;
        if ((Float.valueOf(tempWeight) <= secondClassCheck.getWeightLimit())
                && (Float.valueOf(tempSize) <= secondClassCheck.getSizeLimit())) {
            packageClassCheck = "2";
            firstClassToggle.setSelected(false);
            thirdClassToggle.setSelected(false);
            classErrorLabel.setText("");
        }
        else{
            secondClassToggle.setSelected(false);
            classErrorLabel.setText("Esine ei sovi tähän luokkaan, lisää tietoa infonapista.");
        }
    }

    @FXML
    private void thirdClassSelectionAction(ActionEvent event) {
        float tempSize= Float.valueOf(tempWidth)*Float.valueOf(tempHeight)*Float.valueOf(tempDepth)/1000;
        if ((Float.valueOf(tempWeight) <= thirdClassCheck.getWeightLimit())
                && (Float.valueOf(tempSize) <= thirdClassCheck.getSizeLimit())) {
            packageClassCheck = "3";
            firstClassToggle.setSelected(false);
            secondClassToggle.setSelected(false);
            classErrorLabel.setText("");
        }
        else{
            thirdClassToggle.setSelected(false);
            classErrorLabel.setText("Esine ei sovi tähän luokkaan, lisää tietoa infonapista.");
        }
    }

    @FXML
    private void confirmItemSelectionAction(ActionEvent event) {
        //select object from default objects and move to select package class
        if (defaultObjects.getValue() != null){
            tempName = defaultObjects.getValue();
            for (int i = 0; i < main.getDefaultObjectsList().size(); i++) {
                if (tempName.equals(main.getDefaultObjectsList().get(i).getName())) {
                    tempWeight = main.getDefaultObjectsList().get(i).getWeight();
                    tempWidth = main.getDefaultObjectsList().get(i).getWidth();
                    tempHeight = main.getDefaultObjectsList().get(i).getHeight();
                    tempDepth = main.getDefaultObjectsList().get(i).getDepth();
                    fragile=main.getDefaultObjectsList().get(i).getFragile();
                }
            }

        
            myTabPane.getSelectionModel().select(classSelectionTab);
            classSelectionTab.setDisable(false);
            itemSelectionTab.setDisable(true);
            //set visibility as it was when window was opened
            presetItemSelectButton.setDisable(false);
            createNewObjectButton.setDisable(false);
            //disable selection fields
            defaultObjects.setVisible(false);
            defaultObjects.setDisable(true);
            confirmItemSelectionButton.setDisable(true);
            confirmItemSelectionButton.setVisible(false);
            objectNameField.setVisible(false);
            objectWeightField.setVisible(false);
            objectWidthField.setVisible(false);
            objectDepthField.setVisible(false);
            objectHeightField.setVisible(false);
            fragileCheck.setVisible(false);
            fragileCheck.setDisable(true);
            backToFirstChoiseButton.setDisable(true);
            backToFirstChoiseButton.setVisible(false);
            confirmItemButton.setDisable(true);
            confirmItemButton.setVisible(false);
            backToFirstChoiseButton2.setDisable(true);
            backToFirstChoiseButton2.setVisible(false);
        
        }
        else{
            objectErrorLabel.setText("Valitse esine.");
        }
    }

    @FXML
    private void backToBeginningAction(ActionEvent event) {
        //goes back to the first tab from the last tab
        myTabPane.getSelectionModel().select(itemSelectionTab);
        reviewTab.setDisable(true);
        itemSelectionTab.setDisable(false);
        
    }

    @FXML
    private void backToLocationTabAction(ActionEvent event) {
        myTabPane.getSelectionModel().select(locationSelectionTab);
        reviewTab.setDisable(true);
        locationSelectionTab.setDisable(false);
             
    }

    @FXML
    private void backToClassSelectionAction(ActionEvent event) {
        myTabPane.getSelectionModel().select(classSelectionTab);
        locationSelectionTab.setDisable(true);
        classSelectionTab.setDisable(false);
        sendingMachineCombobox.setDisable(true);
        destinationMachineCombobox.setDisable(true);
        sendingCityCombobox.setDisable(false);
        destinationCityCombobox.setDisable(false);
        confirmCitiesButton.setDisable(false);
        locationSelectionTab.setDisable(true);
        sendingCityCombobox.setDisable(false);
        destinationCityCombobox.setDisable(false);
        sendingMachineCombobox.setDisable(true);
        destinationMachineCombobox.setDisable(true);
        confirmMachineButton.setDisable(true);
        cancelMachineButton.setDisable(true);
        sendingMachineCombobox.getItems().clear();
        destinationMachineCombobox.getItems().clear();
        
    }
    
}
