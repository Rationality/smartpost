/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;

/**
 *
 * @author Ratio
 */
public class Object {

    protected String name;
    protected String width;
    protected String height;
    protected String depth;
    protected String weight;
    protected String fragile;

    public Object(String n, String w, String wi, String he, String de, String f) {
        name = n;
        weight = w;
        width = wi;
        height = he;
        depth = de;
        fragile = f;
    }

    public String getName() {
        return name;
    }

    public String getFragile() {
        return fragile;
    }

    public String getWeight() {
        return weight;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    public String getDepth() {
        return depth;
    }
}

class Shelf extends Object {

    private String material;
    private String colour;

    public Shelf(String n, String w, String wi, String he, String de, String f) {
        super(n, w, wi, he, de, f);
        material = "Wood";
        colour = "Brown";
    }

    public String getColour() {
        return colour;
    }

    public String getMaterial() {
        return material;
    }

}

class Glass extends Object {

    private String material;
    private String colour;

    public Glass(String n, String w, String wi, String he, String de, String f) {
        super(n, w, wi, he, de, f);
        material = "glass";
        colour = "transparent";
    }

    public String getColour() {
        return colour;
    }

    public String getMaterial() {
        return material;
    }

}

class DVD extends Object {

    private String movie;

    public DVD(String n, String w, String wi, String he, String de, String f) {
        super(n, w, wi, he, de, f);
        movie = "Titanic";
    }

    public String getMovie() {
        return movie;
    }
}

class Laptop extends Object {

    private String brand;

    public Laptop(String n, String w, String wi, String he, String de, String f) {
        super(n, w, wi, he, de, f);
        brand = "Acer";
    }

    public String getBrand() {
        return brand;
    }

}
