/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.script.ScriptException;

/**
 *
 * @author Ratio
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private Label label;
    @FXML
    private WebView web;
    @FXML
    private Button createPackageButton;
    @FXML
    private ComboBox<String> cityCombobox;
    @FXML
    private Button addToMapButton;
    @FXML
    private Button clearRoutesButton;
    @FXML
    private Button loadPackagesButton;
    @FXML
    private ComboBox<String> packageCombobox;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Label distanceErrorLabel;
    @FXML
    private Button openMusicButton;
    
    Mainclass main=Mainclass.getInstance();
    XMLtoSmartPost xml= XMLtoSmartPost.getInstance();
    Storage storage= Storage.getInstance();
    @FXML
    private Button showStorageButton;



    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        cityCombobox.getItems().addAll(xml.getCityList());
       
    }    

    @FXML
    private void createPackageAction(ActionEvent event) {
        //Opens new window for creating a package
        try {
            Stage packageCreation = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageManagement.fxml"));
            Scene scene = new Scene(page);
            packageCreation.setMaxHeight(380);
            packageCreation.setMinHeight(380);
            packageCreation.setMinWidth(566);
            packageCreation.setMaxWidth(566);
            packageCreation.setScene(scene);
            packageCreation.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @FXML
    private void addToMapAction(ActionEvent event) { // adds marker(s) to map
        String name=cityCombobox.getValue();
        ArrayList<Smartpost> smartposts = xml.getSmartpostList();
        for(Smartpost s : smartposts){
            if(s.getCity().equals(name)){
            web.getEngine().executeScript("document.goToLocation('"+s.getAddress()
                    +", "+s.getPostnumber()+" "+s.getCity()+"', '"+s.getPostoffice()
                    +", Auki: "+s.getAvailability()
                    +"', 'blue')");
            }
        }
        
    }

    @FXML
    private void clearRoutesAction(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
        
    }

    @FXML
    private void loadPackagesAction(ActionEvent event) { //loads current set of packages to combobox
        ArrayList<Package> packages = storage.getPackageList();
           packageCombobox.getItems().clear();
           for(Package p : packages){
                packageCombobox.getItems().add(p.getName()+"    "+ p);  

           }
     
    }

    @FXML
    private void sendPackageAction(ActionEvent event) throws ScriptException {
        ArrayList<Package> packages = storage.getPackageList();
        ArrayList<Smartpost> smartposts=xml.getSmartpostList();
        double distance=0;
        for(Package p : packages){
               if((p.getName()+"    "+ p).equals(packageCombobox.getValue())){
                   //tein index.html tiedostoon uuden funktion jolla saadaan palautettua matka ilman piirtoa
                   distance=(double)web.getEngine().executeScript("document.getDistance("
                           +main.getCoordinates(p.getSendingMachine(), p.getDestinationMachine())+")");
                   
                   if((distance>150)&&(p.getClass().getSimpleName().equals("firstClass"))){
                       distanceErrorLabel.setText("Matka liian pitkä 1. luokan paketille, poistetaan paketti.");
                       storage.removePackage(p);
                       break;
                       
                   }
                   else{
                        web.getEngine().executeScript("document.createPath("
                        + main.getCoordinates(p.getSendingMachine(), p.getDestinationMachine())
                        + ", 'red', " + main.getPostClass(p) + ")");
                        storage.removePackage(p);
                        break;
                   }
               }
           }
        ArrayList<Package> packages2 = storage.getPackageList(); //päivittää comboboxin
        packageCombobox.getItems().clear();
        packageCombobox.setValue("");
        for (Package p : packages2) {
            packageCombobox.getItems().add(p.getName() + "    " + p);

        }

            
        }


    @FXML
    private void openMusicAction(ActionEvent event) {
        try {
            Stage musicStage = new Stage();
            Parent musicPage = FXMLLoader.load(getClass().getResource("FXMLmusicDocument.fxml"));
            Scene musicScene = new Scene(musicPage);
            musicStage.setScene(musicScene);
            musicStage.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void showStorageAction(ActionEvent event) {
        try {
            Stage storage = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLStorageDocument.fxml"));
            Scene scene = new Scene(page);
            storage.setMaxHeight(450);
            storage.setMinHeight(450);
            storage.setMinWidth(645);
            storage.setMaxWidth(645);
            storage.setScene(scene);
            storage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
  
        
    


}


