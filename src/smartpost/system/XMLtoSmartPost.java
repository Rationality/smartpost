/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.Doc;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Ratio
 */
public class XMLtoSmartPost {
    private ArrayList<Smartpost> SmartpostList = new ArrayList<Smartpost>();
    private ArrayList<String> cityList = new ArrayList<String>();
    private Document doc;
    private static XMLtoSmartPost xml= null;
    
    private XMLtoSmartPost(){
        
    }
    
    public static XMLtoSmartPost getInstance(){
        if(xml==null){
            xml=new XMLtoSmartPost();
        }
        return xml;
        
    }
    
    
   // This area contains methods for loading and parsing data from given URL: 
    
    private String loadSmartpostInfo(){  //Loads smartpost info from URL for parser
         
        try{ URL URL = new URL("http://smartpost.ee/fi_apt.xml");
        
            BufferedReader br = new BufferedReader(new InputStreamReader(URL.openStream()));
            String content = "";
            String line;
        
            while((line=br.readLine())!=null){
                content+=line+"\n";
            }
        
        return content;
        
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public void getSmartposts(){ //Call this method to load data from URL
         String content=loadSmartpostInfo();
         try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            parseCityData();
            parseSmartpostData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Smartpost.class.getName()).log(Level.SEVERE, null, ex);
        }                   
         
          
     }
         
        private void parseCityData(){ // parses data and adds parsed city to cityList
            NodeList Nodes = doc.getElementsByTagName("place");
            for(int i=0;i<Nodes.getLength();i++){
                Node node = Nodes.item(i);
                Element e = (Element) node;
                String city=getValue("city",e);
                if(!cityList.isEmpty()){
                    if(!city.equals(cityList.get(cityList.size()-1))){ //check if city is already in the list
                        cityList.add(city);
                    }
                }
                else{
                    cityList.add(city);
                }
            }
         }
        
        private void parseSmartpostData(){ //parses data and adds Smartpost to SmartpostList
            NodeList Nodes = doc.getElementsByTagName("place");
            for(int i=0;i<Nodes.getLength();i++){
                Node node = Nodes.item(i);
                Element e = (Element) node;
                Smartpost s = new Smartpost(getValue("city",e),getValue("address",e)
                ,getValue("lat",e),getValue("lng",e),getValue("code",e)
                        ,getValue("availability",e),getValue("postoffice",e));
                SmartpostList.add(s);
            }
         }
         
        private String getValue(String tag, Element e){
            return (e.getElementsByTagName(tag).item(0).getTextContent());
        
    }
        
        
        //This area contains getters
        
        public ArrayList getCityList(){
            return cityList;
        }
        
        public ArrayList getSmartpostList(){
            return SmartpostList;
          
        }
        
        public int getSmartPostListSize(){
            return SmartpostList.size();
        }
        
        public Smartpost getSmartpost(int i){
            return SmartpostList.get(i);
        }
        
        
}    
    
    


