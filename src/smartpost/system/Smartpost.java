/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;

/**
 *
 * @author Ratio
 */
public class Smartpost {
    private String city;
    private String address;
    private String postnumber;
    private String postoffice; //Postitoimipaikka
    private String latitude;
    private String longitude;
     private String availability;   
    
    public String getPostoffice() {
        return postoffice;
    }


    public String getAvailability() {
        return availability;
    }

    public String getAddress() {
        return address;
    }

    public String getPostnumber() {
        return postnumber;
    }



    
    public Smartpost(String c,String a,String la,String lo,String pn,String ava,String po ){
        city=c; 
        address=a;
        latitude=la;
        longitude=lo;
        postnumber=pn;
        postoffice=po;
        availability=ava;
        
    }
    

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
    
    
    
    public String getCity(){
        return city;
    }
    
    
}
