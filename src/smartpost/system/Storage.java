/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost.system;
 
import java.util.ArrayList;
import static smartpost.system.Mainclass.mc;
 
/**
 *
 * @author Ratio
 */
public class Storage {
    static public Storage st=null;
    private ArrayList<Package> packages = new ArrayList<Package>();
   
    private Storage (){
 
    }
    
    
    public static Storage getInstance(){
        if (st==null){
           st=new Storage();
        }      
        return st;
    }
   
    public void addPackage(String name, String weight, String Width, String Height,
            String Depth, String fragile, String packageclass,String sendingCity,
            String destinationCity, String sendingMachine, String destinationMachine){
       
        if(packageclass.equals("1")){
            firstClass fc = new firstClass(weight, name, fragile, sendingCity,
                destinationCity, sendingMachine, destinationMachine, Width, Height, Depth);
            packages.add(fc);
        }else if (packageclass.equals("2")) {
            secondClass sc = new secondClass(weight, name, fragile, sendingCity,
                destinationCity, sendingMachine, destinationMachine, Width, Height, Depth);
            packages.add(sc);
        }else if (packageclass.equals("3")) {
            thirdClass tc = new thirdClass(weight, name, fragile, sendingCity,
                    destinationCity, sendingMachine, destinationMachine, Width, Height, Depth);
            packages.add(tc);
        }
    }
   
    public ArrayList<Package> getPackageList(){
        return packages;
    }
    
    public void removePackage(Package p){
        packages.remove(p);
    }
    
    public int getPackageListSize(){
        return packages.size();
    }
   
}